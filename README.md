# koh-frais

Ce dépôt contient le site web ainsi que le design promouvant un vendeur de glace.

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description
Cette SAE a eu pour but de nous apprendre à travailler en groupe ainsi que nous apprendre le HTML, le CSS ainsi que le framework Bootstrap.

La SAE 1.05 du site web s'est faite en 5 étapes :
  * 1. Nous avons, à l'aide d'un texte, identifiez les besoins métiers du client pour retirer le plus d'informations utiles.
  * 2. Ensuite, nous avons réalisé la charte graphique.
  * 3. Et puis juste après le maquettage du site internet.
  * 4. Vient l'étape du codage où nous avons développé ce site en HTML, CSS en utilisant bootstrap.
  * 5. Pour conclure cette SAE, nous avons réalisé une soutenance orale de notre projet.

## Apprentissage critique

AC 15.01 : Appréhender les besoins du client et de l’utilisateur<br>
AC 15.02 : Mettre en place les outils de gestion de projet - Utiliser un outil collaboratif<br>
AC 15.03 : Identifier les acteurs et les différentes phases d'un cycle de développement

## Langage et Framework 
  * HTML
  * CSS
  * Bootstrap
  * Git

## Prérequis 
Afin de pouvoir exécuter l'application sur votre poste, vous devrez avoir :
  * Un navigateur web 

## Exécution
Pour lancer le site, il vous suffit de télécharger le site web et de lancer le fichier `accueil.html`. 